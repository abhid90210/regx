###########################################################################
######################## Linear Regression ################################
###########################################################################
CustomRegression2 <- function (y,a,b,c,d,e,f)
{  
  
  if(exists("DF"))
  {
    rm("DF")  
  }
  DF <- list()
  DF[[1]] <- a
	if(!missing(b)){  DF[[2]] <- b }else{ b <- 0 }
	if(!missing(c)){  DF[[3]] <- c }else{ c <- 0 }
	if(!missing(d)){  DF[[4]] <- d }else{ d <- 0 }
	if(!missing(e)){  DF[[5]] <- e }else{ e <- 0 }
	if(!missing(f)){  DF[[6]] <- f }else{ f <- 0 }
  
  
  for(i in c(1:length(DF)))
  {
    if(ncol(DF[[i]]) == 1)
    {
      colnames(DF[[i]]) <- paste0(substr(names(DF[[i]]), 1, nchar(names(DF[[i]]))-1), "xxxxxxxx", collapse = NULL)
    }
  }
  
  
  n <- length(DF)
	id <- unlist(lapply(1:n, function(i)combn(1:n,i,simplify=F)), recursive=F)
	
   
for(x in id)
{
    time1 = Sys.time()
    switch(length(x),
        CustomRegression("var1", substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), y, DF[[x[1]]]),
        CustomRegression("var2", paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8)), y, DF[[x[1]]], DF[[x[2]]]),
        CustomRegression("var3", paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8)), y, DF[[x[1]]], DF[[x[2]]], DF[[x[3]]]),
        CustomRegression("var4", paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8), "_", substr(names(DF[[x[4]]])[1], 1, nchar(names(DF[[x[4]]])[1])-8)), y, DF[[x[1]]], DF[[x[2]]], DF[[x[3]]], DF[[x[4]]]),
        CustomRegression("var5", paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8), "_", substr(names(DF[[x[4]]])[1], 1, nchar(names(DF[[x[4]]])[1])-8), "_", substr(names(DF[[x[5]]])[1], 1, nchar(names(DF[[x[5]]])[1])-8)), y, DF[[x[1]]], DF[[x[2]]], DF[[x[3]]], DF[[x[4]]], DF[[x[5]]]),
        CustomRegression("var6", paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8), "_", substr(names(DF[[x[4]]])[1], 1, nchar(names(DF[[x[4]]])[1])-8), "_", substr(names(DF[[x[5]]])[1], 1, nchar(names(DF[[x[5]]])[1])-8), "_", substr(names(DF[[x[6]]])[1], 1, nchar(names(DF[[x[6]]])[1])-8)), y, DF[[x[1]]], DF[[x[2]]], DF[[x[3]]], DF[[x[4]]], DF[[x[5]]], DF[[x[6]]])
    )
    
    switch(length(x),
           {
              modelName = substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8)
              timeTaken = round(as.numeric(Sys.time()-time1, units="secs"), 3)
              print(paste(modelName, "-", timeTaken))
            },
           {
             modelName = paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8))
             timeTaken = round(as.numeric(Sys.time()-time1, units="secs"), 3)
             print(paste(modelName, "-", timeTaken))
             },
          {
            modelName = paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8))
            timeTaken = round(as.numeric(Sys.time()-time1, units="secs"), 3)
            print(paste(modelName, "-", timeTaken))
           },
          {
           modelName = paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8), "_", substr(names(DF[[x[4]]])[1], 1, nchar(names(DF[[x[4]]])[1])-8))
           timeTaken = round(as.numeric(Sys.time()-time1, units="secs"), 3)
           print(paste(modelName, "-", timeTaken))
           },
          {
           modelName = paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8), "_", substr(names(DF[[x[4]]])[1], 1, nchar(names(DF[[x[4]]])[1])-8), "_", substr(names(DF[[x[5]]])[1], 1, nchar(names(DF[[x[5]]])[1])-8))
           timeTaken = round(as.numeric(Sys.time()-time1, units="secs"), 3)
           print(paste(modelName, "-", timeTaken))
           },
          {
           modelName = paste(substr(names(DF[[x[1]]])[1], 1, nchar(names(DF[[x[1]]])[1])-8), "_", substr(names(DF[[x[2]]])[1], 1, nchar(names(DF[[x[2]]])[1])-8), "_", substr(names(DF[[x[3]]])[1], 1, nchar(names(DF[[x[3]]])[1])-8), "_", substr(names(DF[[x[4]]])[1], 1, nchar(names(DF[[x[4]]])[1])-8), "_", substr(names(DF[[x[5]]])[1], 1, nchar(names(DF[[x[5]]])[1])-8), "_", substr(names(DF[[x[6]]])[1], 1, nchar(names(DF[[x[6]]])[1])-8))
           timeTaken = round(as.numeric(Sys.time()-time1, units="secs"), 3)
           print(paste(modelName, "-", timeTaken))
          }
           )
}
  
  
  
}


