getwd()
setwd("C:/Documents and Settings/user/Desktop/Regression/tmp")
getwd()
source(file = "adstock.r")
source(file = "regression.r")
source(file = "regression2.r")

# E.g
test = read.csv("data.csv", header = TRUE)
#attach(test)
dep = test[,2]
adstock(3, 17,test,0.2,0.8,0.2,0.2,0.8,0.2)

#########
sfTotal = read.csv("Adstock_SF.tot.GRP_even.csv",header= TRUE )
sfCream = read.csv("Adstock_SF.cream.GRP_even.csv",header= TRUE )
sfHalo = read.csv("Adstock_SF.Halo.GRP_even.csv",header= TRUE )
sfDarkFantasy = read.csv("Adstock_SUNFEAST.DARK.FANTASY_even.csv",header= TRUE )
sfDreamCream = read.csv("Adstock_SUNFEAST.DREAM.CREAM.BISCUITS_even.csv",header= TRUE )

britTotal = read.csv("Adstock_Brit.Tot.GRp_even.csv",header= TRUE )
britCream = read.csv("Adstock_Brit.Cream.GRp_even.csv",header= TRUE )
britHalo = read.csv("Adstock_Brit.Halo.GRP_even.csv",header= TRUE )

parleTotal = read.csv("Adstock_Parle.Tot.GRp_even.csv",header= TRUE )
parleCream = read.csv("Adstock_Parle.Cream.GRP_even.csv",header= TRUE )
parleHalo = read.csv("Adstock_Parle..Halo.GRP_even.csv",header= TRUE )

mainCompTotal = read.csv("Adstock_Comp..tot.GRP_even.csv",header= TRUE )
mainCompCream = read.csv("Adstock_Comp.cream.GRP_even.csv",header= TRUE )

pureCat = read.csv("Adstock_Pure.Cat_even.csv",header= TRUE )

notad = read.csv("notAdstock.csv",header= TRUE )

dummy = read.csv("dummy.csv", header=TRUE)

##############################
CustomRegression("var2", "out_sfTotAwareness_vs_sfTotal_pureCat", dep, sfTotal, pureCat)
CustomRegression("var3", "out_sfTotAwareness_vs_sfCream_sfHalo_pureCat", dep, sfCream, sfHalo, pureCat)
CustomRegression("var3", "out_sfTotAwareness_vs_sfDarkFantasy_sfDreamCream_pureCat", dep, sfDarkFantasy, sfDreamCream, pureCat)
CustomRegression("var4", "out_sfTotAwareness_vs_sfDarkFantasy_sfDreamCream_sfHalo_pureCat", dep, sfDarkFantasy, sfDreamCream, sfHalo, pureCat)
CustomRegression("var4", "out_sfTotAwareness_vs_sfDarkFantasy_sfDreamCream_sfHalo_parleCream_britCream", dep, sfDarkFantasy, sfDreamCream, pureCat, sfHalo)
CustomRegression("var5", "out_sfTotAwareness_vs_sfDarkFantasy_sfDreamCream_sfHalo_parleCream_britCream", dep, sfDarkFantasy, sfDreamCream, sfHalo, parleCream, britCream)


CustomRegression2(dep, sfDarkFantasy)
CustomRegression2(dep, sfDarkFantasy, sfDreamCream)
CustomRegression2(dep, sfDarkFantasy, sfDreamCream, pureCat)
CustomRegression2(dep, sfDarkFantasy, sfDreamCream, pureCat, sfHalo)

t1 <- Sys.time()
CustomRegression2(dep, sfDarkFantasy, sfDreamCream, pureCat, sfHalo)
Sys.time() - t1



