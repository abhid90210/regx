########################Creating AdStocks #################################
#start_adstock = Start col no. 
#end_adstock = End col nol.
#
#
###########################################################################

adstock<- function(start_adstock, end_adstock, inputfile, st_decay, end_decay, inc_decay, st_power, end_power, inc_power)
{
	inputfile=t(inputfile)
	#AdStock Function#
	adstock_new <- function(st_decay, end_decay, inc_decay, st_power, end_power, inc_power,input,x)
	{
		data1<-t(input)
		z<-as.numeric(data1)
		library(MASS)
		myArray <- array(0,dim = c(length(z),0))
		for(i in seq(st_decay, end_decay, inc_decay))
		{
			for(j in seq(st_power, end_power, inc_power))
			{
				y<-c(z[1])
				y1<- c(z[1]^j)
				for(k in 2:length(z))
				{
					new=(z[k]+y[k-1]*i)
					y<-c(y,new)
					y1<- c(y1, new^j)
				}
				t_mat <- t(y1)
				mat<- t(t_mat)
				colnames(mat) <- paste(sep="_",get(deparse(substitute(x))),i,j)

				temp <- mat

				myArray <- cbind(myArray,temp)
			}
		}
		
		x1 = deparse(substitute(x))
		
		if(st_decay %% 0.2== 0 & inc_decay == 0.2)
		{
			output1<- paste("Adstock_",get(x1),"_even", sep="")
		}
		else if(st_decay %% 0.2 != 0 & inc_decay == 0.2)
		{
			output1<- paste("Adstock_",get(x1),"_odd", sep="")
		}
		else
		{
			output1<- paste("Adstock_",get(x1), sep="")
		}
		
		write.matrix(myArray,file= paste(output1,".csv",sep=""),sep = " ,")
	}

	for(i in start_adstock:end_adstock)
	{
		op1 = noquote(rownames(inputfile)[i])
		adstock_new(st_decay,end_decay,inc_decay,st_power,end_power,inc_power,inputfile[i, ],op1)
	}
}

