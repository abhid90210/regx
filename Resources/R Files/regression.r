###########################################################################
######################## Linear Regression #################################
###########################################################################
CustomRegression <- function (option,outputname,y,a,b,c,d,e,f)
{
time1 = Sys.time()
#data frame to matrix
y = t(y)
y = as.vector(y)
# Optional Paramerters #
     if(missing(b)){
     b <- 0
     }else{
     b <- b
     }

     if(missing(c)){
     c <- 0
     }else{
     c <- c
     }

     if(missing(d)){
     d <- 0
     }else{
     d <- d
     }

     if(missing(e)){
     e <- 0
     }else{
     e <- e
     }

     if(missing(f)){
     f <- 0
     }else{
     f <- f
     }

a = as.matrix(a)
b = as.matrix(b)
c = as.matrix(c)
d = as.matrix(d)
e = as.matrix(e)
f = as.matrix(f)

# Required Packages #  
library(foreach)
library(MASS)
library(doSNOW)
cl <- makeCluster(4, type = "SOCK")
registerDoSNOW(cl)

# 1 var function#
Regmodel1 <- function(y,a,filename) {
allvar <- foreach(a1 = 1:NCOL(a),.combine = rbind) %do% {

          r <- lm(y~a[ ,a1])
	
	  	
		    raw <- summary(r)			   
		    coefficients<- coef(raw)
		    coefmatrix<- coefficients[, "Estimate"]
		    tmatrix <- coefficients[, "t value"]
		    

		  ssr <- sum(r$residuals^2) # denominator of DW statistic.
		  ei <- r$residuals[-1] # first element deleted.
		  eim1 <- r$residuals[-15] # last element are deleted
		  ssdr <- (ei -eim1)^2 # sum of squared of differenced of residuals
		  ssdr <- sum((ei -eim1)^2)
		  DW <- ssdr/ ssr

		  cola <- colnames(a)
    
      matrix <- t(c(cola[a1],raw [[8]][[1]],raw [[9]][[1]],raw[[10]][[1]],coefmatrix[[1]],coefmatrix[[2]],tmatrix[[1]],tmatrix[[2]],DW))
	}
  
  
  allvar <- as.data.frame(allvar)

  colnames(allvar)<-c("name_ind1","rsquare","adjr2","fstat","coef_int","coef_var1","t_int","t_ind1","dw")
  x1 = deparse(substitute(filename))
  output1<- paste("OUTPUT_", get(x1))
  write.matrix(allvar,file= paste(output1, ".csv"), sep=",")
}


# 2 var Function#
Regmodel2 <- function(y,a,b,filename)
{
  
	allvar <- foreach(a1 = 1:NCOL(a),.combine = rbind) %:%
	foreach(a2 = 1:NCOL(b) ,.combine = rbind) %dopar% 
	{

		r <- lm(y~a[ ,a1]+b[ ,a2])

		raw <- summary(r)			   
		coefficients<- coef(raw)
		coefmatrix<- coefficients[, "Estimate"]
		tmatrix <- coefficients[, "t value"]

		ssr <- sum(r$residuals^2) # denominator of DW statistic.
		ei <- r$residuals[-1] # first element deleted.
		eim1 <- r$residuals[-15] # last element are deleted
		ssdr <- (ei -eim1)^2 # sum of squared of differenced of residuals
		ssdr <- sum((ei -eim1)^2)
		DW <- ssdr/ ssr

		cola <- colnames(a)
		colb <- colnames(b)
		
		matrix <- c(cola[a1],colb[a2],raw [[8]][[1]],raw [[9]][[1]],raw[[10]][[1]],coefmatrix[[1]],coefmatrix[[2]],coefmatrix[[3]] ,tmatrix[[1]],tmatrix[[2]],tmatrix[[3]],DW)
	}
  

	colnames(allvar)<-c("name_ind1", "name_ind2","rsquare","adjr2","fstat",	"coef_int", "coef_var1", "coef_var2", "t_int", "t_ind1" ,"t_ind2", "dw")

	x1 = deparse(substitute(filename))
	output1<- paste("OUTPUT_", get(x1))
	write.matrix(allvar,file= paste(output1, ".csv"), sep=",")
}



#3 Var function#

Regmodel3 <- function(y,a,b,c,filename) {
allvar <- foreach(a1 = 1:NCOL(a),.combine = rbind, .inorder=FALSE, .packages=c("foreach")) %dopar%
{
   foreach(a2 = 1:NCOL(b) ,.combine = rbind)%:%
     foreach(a3 = 1:NCOL(c) ,.combine = rbind) %do% {

          r <- lm(y~a[ ,a1]+b[ ,a2]+c[ ,a3])
	
	  	
		    raw <- summary(r)			   
		    coefficients<- coef(raw)
		    coefmatrix<- coefficients[, "Estimate"]
		    tmatrix <- coefficients[, "t value"]
		    

		  ssr <- sum(r$residuals^2) # denominator of DW statistic.
		  ei <- r$residuals[-1] # first element deleted.
		  eim1 <- r$residuals[-15] # last element are deleted
		  ssdr <- (ei -eim1)^2 # sum of squared of differenced of residuals
		  ssdr <- sum((ei -eim1)^2)
		  DW <- ssdr/ ssr

		  cola <- colnames(a)
                  colb <- colnames(b)
                  colc <- colnames(c)
                 
  matrix <- c(cola[a1],colb[a2],colc[a3],raw [[8]][[1]],raw [[9]][[1]],raw[[10]][[1]]
	     ,coefmatrix[[1]],coefmatrix[[2]],coefmatrix[[3]],coefmatrix[[4]]
	     ,tmatrix[[1]],tmatrix[[2]],tmatrix[[3]],tmatrix[[4]],DW)

	}
}

colnames(allvar)<-c("name_ind1", "name_ind2","name_ind3","rsquare","adjr2","fstat",
	                    "coef_int","coef_var1","coef_var2","coef_var3","t_int","t_ind1"
			    ,"t_ind2","t_ind3","dw")

x1 = deparse(substitute(filename))
output1<- paste("OUTPUT_", get(x1))
write.matrix(allvar,file= paste(output1, ".csv"), sep=",")

}


#4 Var function#

Regmodel4 <- function(y,a,b,c,d,filename) {
allvar <- foreach(a1 = 1:NCOL(a),.combine = rbind, .inorder=FALSE, .packages=c("foreach")) %dopar%
{
   foreach(a2 = 1:NCOL(b) ,.combine = rbind) %do%
  {
      foreach(a3 = 1:NCOL(c) ,.combine = rbind) %:%
        
         foreach(a4 = 1:NCOL(d) ,.combine = rbind)  %do%
         {

          r <- lm(y~a[ ,a1]+b[ ,a2]+c[ ,a3]+d[ ,a4])
	
	  	
		    raw <- summary(r)			   
		    coefficients<- coef(raw)
		    coefmatrix<- coefficients[, "Estimate"]
		    tmatrix <- coefficients[, "t value"]
		    

		  ssr <- sum(r$residuals^2) # denominator of DW statistic.
		  ei <- r$residuals[-1] # first element deleted.
		  eim1 <- r$residuals[-15] # last element are deleted
		  ssdr <- (ei -eim1)^2 # sum of squared of differenced of residuals
		  ssdr <- sum((ei -eim1)^2)
		  DW <- ssdr/ ssr

		  cola <- colnames(a)
                  colb <- colnames(b)
                  colc <- colnames(c)
		  cold <- colnames(d)

 
		  
 matrix <- c(cola[a1],colb[a2],colc[a3],cold[a4],raw [[8]][[1]],raw [[9]][[1]],raw[[10]][[1]]
	     ,coefmatrix[[1]],coefmatrix[[2]],coefmatrix[[3]],coefmatrix[[4]],coefmatrix[[5]]
	     ,tmatrix[[1]],tmatrix[[2]],tmatrix[[3]],tmatrix[[4]],tmatrix[[5]],DW)
	  }
  }
}

colnames(allvar)<-c("name_ind1", "name_ind2","name_ind3","name_ind4","rsquare","adjr2","fstat",
	                    "coef_int","coef_var1","coef_var2","coef_var3","coef_var4","t_int","t_ind1"
			    ,"t_ind2","t_ind3","t_ind4", "dw")

  x1 = deparse(substitute(filename))
  output1<- paste("OUTPUT_", get(x1))
 write.matrix(allvar,file= paste(output1, ".csv"), sep=",")

}

#5 Var function#

Regmodel5 <- function(y,a,b,c,d,e,filename) {
allvar <- foreach(a1 = 1:NCOL(a), .combine = rbind, .inorder=FALSE, .packages=c("foreach")) %dopar%
{
   foreach(a2 = 1:NCOL(b) ,.combine = rbind) %dopar%
   {
      foreach(a3 = 1:NCOL(c) ,.combine = rbind) %do%
	  {
         foreach(a4 = 1:NCOL(d) ,.combine = rbind) %:%
	     foreach(a5 = 1:NCOL(e) ,.combine = rbind) %do% {

          r <- lm(y~a[ ,a1]+b[ ,a2]+c[ ,a3]+d[ ,a4] +e[ ,a5])
	
	  	
		    raw <- summary(r)			   
		    coefficients<- coef(raw)
		    coefmatrix<- coefficients[, "Estimate"]
		    tmatrix <- coefficients[, "t value"]

		  ssr <- sum(r$residuals^2) # denominator of DW statistic.
		  ei <- r$residuals[-1] # first element deleted.
		  eim1 <- r$residuals[-15] # last element are deleted
		  ssdr <- (ei -eim1)^2 # sum of squared of differenced of residuals
		  ssdr <- sum((ei -eim1)^2)
		  DW <- ssdr/ ssr

		  cola <- colnames(a)
                  colb <- colnames(b)
                  colc <- colnames(c)
		  cold <- colnames(d)
		  cole <- colnames(e)

 matrix <- c(cola[a1],colb[a2],colc[a3],cold[a4],cole[a5],raw [[8]][[1]],raw [[9]][[1]],raw[[10]][[1]]
	     ,coefmatrix[[1]],coefmatrix[[2]],coefmatrix[[3]],coefmatrix[[4]],coefmatrix[[5]],coefmatrix[[6]]
	     ,tmatrix[[1]],tmatrix[[2]],tmatrix[[3]],tmatrix[[4]],tmatrix[[5]],tmatrix[[6]],DW)



		}
		}
	}
}
colnames(allvar)<-c("name_ind1", "name_ind2","name_ind3","name_ind4","name_ind5","rsquare","adjr2","fstat",
	                    "coef_int","coef_var1","coef_var2","coef_var3","coef_var4","coef_var5","t_int","t_ind1"
			    ,"t_ind2","t_ind3","t_ind4","t_ind5", "dw")


  x1 = deparse(substitute(filename))
  output1<- paste("OUTPUT_", get(x1))

 write.matrix(allvar,file= paste(output1,"_allvar",".csv"), sep=",")

}



#6 Var function#

Regmodel6 <- function(y,a,b,c,d,e,f,filename) {

allvar <- foreach(a1 = 1:NCOL(a),.combine = rbind) %:%
   foreach(a2 = 1:NCOL(b) ,.combine = rbind) %:%
      foreach(a3 = 1:NCOL(c) ,.combine = rbind) %:%
         foreach(a4 = 1:NCOL(d) ,.combine = rbind) %:%
	  foreach(a5 = 1:NCOL(e) ,.combine = rbind) %:%
	     foreach(a6 = 1:NCOL(f) ,.combine = rbind) %dopar% {

          r <- lm(y~a[ ,a1]+b[ ,a2]+c[ ,a3]+d[ ,a4] +e[ ,a5]+ f[ ,a6])
	
	  	
		    raw <- summary(r)			   
		    coefficients<- coef(raw)
		    coefmatrix<- coefficients[, "Estimate"]
		    tmatrix <- coefficients[, "t value"]

		  ssr <- sum(r$residuals^2) # denominator of DW statistic.
		  ei <- r$residuals[-1] # first element deleted.
		  eim1 <- r$residuals[-15] # last element are deleted
		  ssdr <- (ei -eim1)^2 # sum of squared of differenced of residuals
		  ssdr <- sum((ei -eim1)^2)
		  DW <- ssdr/ ssr

		  cola <- colnames(a)
                  colb <- colnames(b)
                  colc <- colnames(c)
		  cold <- colnames(d)
		  cole <- colnames(e)
		  colf <- colnames(f)

 matrix <- c(cola[a1],colb[a2],colc[a3],cold[a4],cole[a5],colf[a6],raw [[8]][[1]],raw [[9]][[1]],raw[[10]][[1]]
	     ,coefmatrix[[1]],coefmatrix[[2]],coefmatrix[[3]],coefmatrix[[4]],coefmatrix[[5]],coefmatrix[[6]],coefmatrix[[7]]
	     ,tmatrix[[1]],tmatrix[[2]],tmatrix[[3]],tmatrix[[4]],tmatrix[[5]],tmatrix[[6]],tmatrix[[7]],DW)



	}
colnames(allvar)<-c("name_ind1", "name_ind2","name_ind3","name_ind4","name_ind5","name_ind6","rsquare","adjr2","fstat",
	                    "coef_int","coef_var1","coef_var2","coef_var3","coef_var4","coef_var5","coef_var6","t_int","t_ind1"
			    ,"t_ind2","t_ind3","t_ind4","t_ind5","t_ind6", "dw")


  x1 = deparse(substitute(filename))
  output1<- paste("OUTPUT_", get(x1))

 write.matrix(allvar,file= paste(output1,"_allvar",".csv"), sep=",")

}

###### Switch Case ######

    #op1 = deparse(substitute(outputname))
    op1 = outputname

	switch(option,
		var1 = Regmodel1(y,a,op1),
		var2 = Regmodel2(y,a,b,op1),
		var3 = Regmodel3(y,a,b,c,op1),
		var4 = Regmodel4(y,a,b,c,d,op1),
		var5 = Regmodel5(y,a,b,c,d,e,op1),
		var6 = Regmodel6(y,a,b,c,d,e,f,op1)
	)
      
#End of function #
timeTaken = as.numeric(Sys.time()-time1, units="secs")
stopCluster(cl)
numModels = NCOL(a)*NCOL(b)*NCOL(c)*NCOL(d)*NCOL(e)*NCOL(f)
modelRate = round( numModels / timeTaken, 2)
paste("Time taken:", as.difftime(Sys.time()-time1, "%m/%d/%y %H:%M:%S") , "| Start Time:", format(time1, "%a %b %d %X %Y %Z"), "|", numModels, "Models Done @", modelRate, "Models/Sec")

}


